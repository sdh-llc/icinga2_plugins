#!/bin/bash

username=
password=

## Function helpers
Usage() {
cat << EOF

Required parameters:
  -4 HOSTADDRESS (\$host.address\$)
  -l HOSTNAME (\$host.name\$)
  -o HOSTOUTPUT (\$host.output\$)
  -p CONTACTPAGER (\$user.pager\$)
  -s HOSTSTATE (\$host.state\$)
  -t NOTIFICATIONTYPE (\$notification.type\$)

EOF
}

Help() {
  Usage;
  exit 0;
}

Error() {
  if [ "$1" ]; then
    echo $1
  fi
  Usage;
  exit 1;
}

## Main
while getopts 4:6:p:l:o:s:t: opt
do
  case "$opt" in
    4) HOSTADDRESS=$OPTARG ;;
    6) HOSTADDRESS6=$OPTARG ;;
    h) Help ;;
    l) HOSTNAME=$OPTARG ;; # required
    o) HOSTOUTPUT=$OPTARG ;; # required
    p) CONTACTPAGER=$OPTARG ;; # required
    s) HOSTSTATE=$OPTARG ;; # required
    t) NOTIFICATIONTYPE=$OPTARG ;; # required
   \?) echo "ERROR: Invalid option -$OPTARG" >&2
       Error ;;
    :) echo "Missing option argument for -$OPTARG" >&2
       Error ;;
    *) echo "Unimplemented option: -$OPTARG" >&2
       Error ;;
  esac
done

shift $((OPTIND - 1))

template=$(cat <<TEMPLATE
*$NOTIFICATIONTYPE-$HOSTNAME-is-$HOSTSTATE*
TEMPLATE
)


/usr/bin/curl --output "/var/log/icinga2/smslog" "https://smsc.ua/sys/send.php?login=$username&psw=$password&phones=$CONTACTPAGER&mes=$template"
