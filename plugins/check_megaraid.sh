#!/bin/bash
# Standard Nagios plugin return codes
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3
#-------
function HELP {
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`
echo -e \\n"Help documentation for ${BOLD}$0${NORM}"\\n
echo "please provide what we should monitor in next format /c0/v0 "
echo -e "Example: ${BOLD}$0 -D /c0/v0${NORM}"

echo "please use  ${BOLD}/opt/MegaRAID/storcli/storcli64 show${NORM} for finding nedeed device"
exit $STATUS_UNKNOWN
}

while getopts :D:h FLAG; do
        case $FLAG in
                D)
                        DISK=$OPTARG
                        ;;
                h)
                        HELP
                        ;;

                        *)
                        HELP
                        ;;
        esac
done

if [[ -z ${DISK}  ]]; then
        HELP
else

        {
        RESULT=$(sudo /opt/MegaRAID/storcli/storcli64 $DISK show)
        VALUE=$(echo "${RESULT}" | grep "RAID" | awk -F " " '{print $3}')
        } || {
        echo "CRITICAL something gone wrong "
        exit $STATUS_UNKNOWN
        }

        if [[ "${VALUE}" == "Optl" ]]; then
                echo "OK, State is : ${VALUE}"
                exit $STATUS_OK
        else
                echo  "CRITICAL State is not Optl:"
                echo "${RESULT}"
                exit $STATUS_CRITICAL
        fi
fi

