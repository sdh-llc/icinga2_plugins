#!/bin/bash

#HELP function
function HELP {
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`
echo -e \\n"Help documentation for ${BOLD}$0${NORM}"\\n
echo "${REV}-M${NORM}  --Sets the ${BOLD}mode of operation. Avaible the next mods 1) for all processes -M all 2) for needed processes -M selected."
echo "${REV}-P${NORM}  --Sets name of processes Example:${BOLD} -P test_uwsgi -P test_twisted -P test_celery."
echo -e "${REV}-h${NORM}  --Displays this ${BOLD}h${NORM}elp message."\\n
echo -e "Example: ${BOLD}$0 -M selected -P test_uwsgi -P test_twisted -P test_celery ${NORM}"
echo -e "Or: ${BOLD}$0 -M all${NORM}"\\n
exit
}


#opt for script
while getopts ":M:P:" opt; do
    case $opt in
        M)
        MODE=$OPTARG
        ;;
        P) multi+=("$OPTARG")
	    ;;
	    h)
	    HELP
	    ;;
	    *)
	    HELP
	    ;;
        esac

    if [ -z $OPTARG ]; then

	    HELP
        exit 1

    fi

    if [ "$MODE" == "all" ];then

    	if [ -z $multi ];then
    		continue

    		else

    		echo "In mode all not accepted select processes"
    		exit 1
    	fi
    fi

done

  if [ "$MODE" == "select" ];then

	if [ -z $multi ];then

		echo "You not select processes, please read help"
		exit 1
	fi
  fi

#command for find process

path="/etc/supervisor/conf.d/*.conf"

downflag=""
downlist=()
timeflag=""
timelist=()
timenameofpid=()

if [ "$MODE" == "all" ]; then

    getpath=$(cat $path | grep "command" |awk -F"[= ]+" '{if ($2!="java") {print $2} else {print $4} }');

    for proc in $getpath
        do
        check_process=$(ps aux | grep $proc | grep -v "grep");

        if [ -z  "$check_process" ];then
        #flag for determinate if some process is down
        downflag="true"

        else
        get_pids=$(pgrep -f $proc);
        for pid in $get_pids; do

        get_time=$(ps -p $pid -o etimes= | awk '{print ($0/60)}')
	    #convert to int
	    gettime=${get_time%.*}

	    if [ "$gettime" -lt "10" ]; then
                timeflag="true"
                timelist+=("$proc")
                timenameofpid+=("$pid")

            fi
        done

        fi

    done

    if [ "$downflag" == "true" ]; then
        #TODO write function to check name of proc
        echo "We have a problem with some process, pls check"
       if [ "$timeflag" == "true" ]; then
            for pid in $timenameofpid
            do
            nameofpid=$(pwdx $pid)
            #echo " PID name $nameofpid"
            done
            echo "And a some processes had less 10 lifetime "
            exit 2
            else
            exit 2
        fi

        else
        echo "All process is running"
         if [ "$timeflag" == "true" ]; then
            echo "But the next processes had less 10 lifetime ${timelist[@]}"
            exit 2
            else
            exit 0
        fi
    fi
fi

if  [ "$MODE" == "select" ]; then

    for val in "${multi[@]}"; do

    getpath=$(cat $path | grep  -A 2 "\[program\:$val\]" | grep "command" | awk -F"[= ]+" '{if ($2!="java") {print $2} else {print $4} }');
    check_process=$(ps aux | grep $getpath | grep -v "grep");
    if [ -z  "$check_process" ];then

        downlist+=("$val")
        downflag="true"
        else
        #TODO finish check lifetime of process
        get_pids=$(pgrep -f $getpath);
        for pid in $get_pids; do

        get_time=$(ps -p $pid -o etimes= | awk '{print ($0/60)}')
	    #convert to int
	    gettime=${get_time%.*}
            #echo "IS INT? $gettime"
	    if [ "$gettime" -lt "10" ]; then
                timeflag="true"
                timelist+=("$val")

            fi
        done

    fi

    done

    if [ "$downflag" == "true" ]; then
        echo "The next process is down ${downlist[@]}"
        if [ "$timeflag" == "true" ]; then
            echo "And the next processes had less 10 lifetime ${timelist[@]}"
            exit 2
            else
            exit 2
        fi

    else
        echo "All selected processes is running"
        if [ "$timeflag" == "true" ]; then
            echo "But the next processes had less 10 lifetime ${timelist[@]}"
            exit 2
            else
            exit 0
        fi

    fi


fi