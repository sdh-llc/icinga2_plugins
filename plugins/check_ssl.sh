#!/bin/bash
set -e
export TERM=xterm-256color

#-------
# HELP |
#-------
function HELP {
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`
echo -e \\n"Help documentation for ${BOLD}$0${NORM}"\\n
echo "${REV}-F${NORM}  --Sets the value for the ${BOLD}f${NORM}ile path. e.g ${BOLD}/etc/ssl/certs/*.crt${NORM}."
echo "${REV}-H${NORM}  --Sets the value for the ${BOLD}h${NORM}ostname. e.g ${BOLD}adfinis-sygroup.ch${NORM}."
echo "${REV}-I${NORM}  --Sets an optional value for an ${BOLD}i${NORM}p to connect. e.g ${BOLD}127.0.0.1${NORM}."
echo "${REV}-S${NORM}  --Sets an optional value for an ${BOLD}S${NORM}tdin."
echo "${REV}-p${NORM}  --Sets the value for the ${BOLD}p${NORM}ort. e.g ${BOLD}443${NORM}."
echo "${REV}-P${NORM}  --Sets an optional value for an TLS ${BOLD}P${NORM}rotocol. e.g ${BOLD}xmpp${NORM}."
echo "${REV}-w${NORM}  --Sets the value for the days before ${BOLD}w${NORM}arning. Default are ${BOLD}30${NORM}."
echo "${REV}-c${NORM}  --Sets the value for the days before ${BOLD}C${NORM}ritical. Default are ${BOLD}5${NORM}."
echo "${REV}-a${NORM}  --Sets the value for the cert validation returning OK n-days ${BOLD}a${NORM}fter expiration"
echo -e "${REV}-h${NORM}  --Displays this ${BOLD}h${NORM}elp message."\\n
echo -e "Example: ${BOLD}$0 -H adfinis-sygroup.ch -p 443 -w 40${NORM}"
echo -e "Or: ${BOLD}$0 -H jabber.adfinis-sygroup.ch -p 5222 -P xmpp -w 30 -c 5${NORM}"\\n
exit 2
}

#---------------
# GET HOSTINFO |
#---------------
while getopts :F:H:I:p:P:w:c:a:h:S FLAG; do
  case $FLAG in
    F) # set file
      FILE=$OPTARG
      ;;

    H) #set host
      HOST=$OPTARG
      ;;

    I) #set ip to connect
      IP=$OPTARG
      ;;

    S) #stdin
      STDIN=true
      ;;

    p) #set port
      PORT=$OPTARG
      ;;

    P) #set tls intended protocol
      if [ "${OPTARG}" == "no_tls" ] || [ -z "${OPTARG}" ]; then
        PROTOCOL=""
      else
        PROTOCOL=$OPTARG
      fi
      ;;

    w) #set day before warning
      WARNING_DAYS=$OPTARG
      ;;

    c) #set day before Critical
      CRITICAL_DAYS=$OPTARG
      ;;

    a) #set day after expiration date
      AFTER_EXPIRATION_OK_DAYS=$OPTARG
      ;;

    h) #show help
      HELP
      ;;

    *)
      HELP
      ;;
  esac
done

if [[ -z "${1}" ]]; then
  read -p "Hostname: " HOST
  read -p "Port (443): " PORT
  if [[ -z "${PORT}" ]]; then
    PORT=443
  else
    read -p "Protocol (leave empty, when It's a SSL Protocol): " PROTOCOL
  fi
fi

if [[ -z "${PORT}" ]]; then
  PORT=443
fi

if [[ -z "${WARNING_DAYS}" ]]; then
  WARNING_DAYS=28
fi

if [[ -z "${CRITICAL_DAYS}" ]]; then
  CRITICAL_DAYS=10
fi

if [[ -z "${HOST}" && -z "${FILE}" && -z "${STDIN}" ]]; then
  HELP
fi

if [[ -z "${IP}" && -z "${STDIN}" ]]; then
  IP=${HOST}
fi

if [[ -z "${AFTER_EXPIRATION_OK_DAYS}" ]]; then
  AFTER_EXPIRATION_OK_DAYS=0
fi

#-----------
# GET DATE |
#-----------
DATE_ACTUALLY_SECONDS=$(date +"%s")

#--------------------------
# CHECK FILE AND HOST SET |
#--------------------------
FAILCNT=0
for PRM in "${HOST}" "${FILE}" "${STDIN}"; do
  if [[ -n "$PRM" ]]; then
    FAILCNT=$(($FAILCNT + 1))
  fi
done
if [[ $FAILCNT -gt 1 ]]; then
    echo "Choose only one from FILE, HOST, STDIN"
    HELP
fi

#----------------------------
# GET CERTIFICATE FROM CITE |
#----------------------------
if [[ "${HOST}" != "" ]]; then
  if [[ -z "${PROTOCOL}" ]]; then
    HOST_CHECK=$(openssl s_client -servername "${HOST}" -connect "${IP}":"${PORT}" 2>&- | openssl x509 -enddate -noout)
    while [ "${?}" = "1" ]; do
      echo "Check Hostname"
      exit 2
    done
    DATE_EXPIRE_SECONDS=$(echo "${HOST_CHECK}" | sed 's/^notAfter=//g' | xargs -I{} date -d {} +%s)
  else
      HOST_CHECK=$(openssl s_client -servername "${HOST}" -connect "${IP}":"${PORT}" -starttls "${PROTOCOL}" 2>&- | openssl x509 -enddate -noout)
    while [ "${?}" = "1" ]; do
      echo "Check Hostname"
      exit 2
    done
    DATE_EXPIRE_SECONDS=$(echo "${HOST_CHECK}" | sed 's/^notAfter=//g' | xargs -I {} date -d {} +%s)
  fi
fi

#----------------------------
# GET CERTIFICATE FROM FILE |
#----------------------------
if [[ "${FILE}" != "" ]]; then
  HOST_CHECK=$(ls ${FILE} | xargs -I{} openssl x509 -enddate -noout -in {})
  DATE_EXPIRE_SECONDS=$(echo "${HOST_CHECK}" | sed 's/^notAfter=//g' | xargs -I {} date -d {} +%s)
fi

#----------------------------
# GET CERTIFICATE FROM STDIN |
#----------------------------
if [[ "${STDIN}" != "" ]]; then
  HOST_CHECK=$(cat | openssl x509 -enddate -subject -noout)
  DATE_EXPIRE_SECONDS=$(echo "${HOST_CHECK}" | grep "notAfter" | sed 's/^notAfter=//g' | xargs -I {} date -d {} +%s)
  COMMON_NAME=$(echo $HOST_CHECK | sed -n '/subject/s/.*CN = //p')
fi

checked_file_name () {
  if [[ "${FILE}" != "" ]]; then
    echo -e `ls ${FILE} | head -n $i | tail -n 1`
  elif [[ -n "${STDIN}" ]]; then
    echo $COMMON_NAME
  fi
}

#-------------------
# DATE CALCULATION |
#-------------------
DATE_EXPIRE_FORMAT=$(echo $DATE_EXPIRE_SECONDS | xargs -I {} -d ' ' date -I --date="@{}")
DATE_DIFFERENCE_SECONDS=$(echo "${DATE_EXPIRE_SECONDS}" | xargs -I {} sh -c "echo '{}-${DATE_ACTUALLY_SECONDS}' | bc")
DATE_DIFFERENCE_DAYS=$(echo "${DATE_DIFFERENCE_SECONDS}" | xargs -I {} sh -c "echo '{}/60/60/24' | bc")

#---------
# OUTPUT |
#---------
DATE_DIFFERENCE_DAYS_ARRAY=($DATE_DIFFERENCE_DAYS)
DATE_EXPIRE_FORMAT_ARRAY=($DATE_EXPIRE_FORMAT)
LOOP_COUNTER=${#DATE_DIFFERENCE_DAYS_ARRAY[@]}

for i in `seq 1 $LOOP_COUNTER`
do
  if [[ ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -gt ${WARNING_DAYS} && ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -ge 0 ]]; then
    checked_file_name
    echo " ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} days left "
    echo -e "OK: Cert will expire on: ${DATE_EXPIRE_FORMAT_ARRAY[$i-1]}\n"
  elif [[ ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -le 0 && ${AFTER_EXPIRATION_OK_DAYS} != 0 && ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -le ${AFTER_EXPIRATION_OK_DAYS} ]]; then
    checked_file_name
    echo -e "OK: Cert expired on: ${DATE_EXPIRE_FORMAT_ARRAY[$i-1]} but this is fine.\n"
    exit 0
  elif [[ ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -le ${CRITICAL_DAYS} && ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -ge 0 ]]; then
    checked_file_name
    echo -e "CRITICAL: Cert will expire on: ${DATE_EXPIRE_FORMAT_ARRAY[$i-1]}\n"
    exit 2
  elif [[ ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -le "${WARNING_DAYS}" && ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -ge 0 ]]; then
    checked_file_name
    echo -e "WARNING: Cert will expire on: ${DATE_EXPIRE_FORMAT_ARRAY[$i-1]}\n"
    exit 1
  elif [[ ${DATE_DIFFERENCE_DAYS_ARRAY[$i-1]} -le 0 ]]; then
    checked_file_name
    echo -e "CRITICAL: Cert expired on: ${DATE_EXPIRE_FORMAT_ARRAY[$i-1]}\n"
    exit 2
  else
    echo -e "CRITICAL: something goes wrong"
    exit 2
  fi
done
