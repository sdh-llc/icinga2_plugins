#!/opt/icinga2_plugins/env/bin/python

import sys
import subprocess
try:
    from queue import Queue
except:
    from Queue import Queue
import threading
import argparse
import os.path
import hvac


EXIT_NAGIOS_OK = 0
EXIT_NAGIOS_WARN = 1
EXIT_NAGIOS_CRITICAL = 2

NUMBER_OF_THREADS = 4
END_RESULT = []


class MultiProc(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            data = self.queue.get()
            self.get_data(data)
            self.queue.task_done()

    def get_data(self, data):
        run_command(data)


def check_result():
    final_status = 0
    for i in END_RESULT:
        if i['status'] > final_status:
            final_status = i['status']
    newlist = sorted(END_RESULT, key=lambda k: k['status'], reverse=True)
    for i in newlist:
        print((i['serial'], i['text']))
    if final_status == 0:
        sys.exit(EXIT_NAGIOS_OK)
    if final_status == 1:
        sys.exit(EXIT_NAGIOS_WARN)
    if final_status == 2:
        sys.exit(EXIT_NAGIOS_CRITICAL)
    if final_status == 3:
        sys.exit(EXIT_NAGIOS_CRITICAL)


def run_command(data):
    status = 0
    try:
        proc = subprocess.Popen(
            data[0], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        output, error = proc.communicate(data[2].encode('utf-8'))
        result = str(output.decode('utf-8'))

    except subprocess.CalledProcessError as e:
        result = str(e.output)
    if 'OK:' not in result:
        if 'WARNING' in result:
            status = 1
        elif 'CRITICAL' in result:
            status = 2
        else:
            status = 3
    END_RESULT.append({'serial': data[1], 'status': status, 'text': result.replace(
        "\\n", "").replace("\n", "")})


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--roleid", help='RoleID for vault auth',
                        type=str,
                        required=True)
    parser.add_argument("--secretid", help='SecretID for vault auth',
                        type=str,
                        required=True)
    parser.add_argument("--certsmountpoint", help='Secrets engine name to check certificates from',
                        type=str,
                        required=True)
    parser.add_argument("--loginmountpoint", help='Auth engine name to login',
                        type=str,
                        required=True)
    parser.add_argument("--cacert", help='CA certificate to check vault',
                        default="/etc/ssl/certs/ca-certificates.crt",
                        type=str)
    parser.add_argument("--url", help='Vault url',
                        type=str,
                        required=True)
    parser.add_argument("-w", "--warn", help='Warn when days left',
                        default="28",
                        type=str)
    parser.add_argument("-c", "--crit", help='Critical when days left',
                        default="7",
                        type=str)
    parser.add_argument("-a", "--afterexp", help='Days after expiration date when certificates become ignored',
                        default="1",
                        type=str)
    parser.add_argument("-l", "--lib_path",
                        default="/usr/lib/nagios/plugins",
                        help='you can provide full path to check_http and check_ssl.sh example -l ./ ',
                        type=str)

    args = parser.parse_args()

if not os.path.isfile(args.lib_path + "/check_ssl.sh"):
    print(("please check check_ssl.sh scripts in " + args.lib_path +
          " or change lib_path for local testing -l new_lib_path"))
    sys.exit(EXIT_NAGIOS_CRITICAL)

queue = Queue()
for i in range(NUMBER_OF_THREADS):
    t = MultiProc(queue)
    t.daemon = True
    t.start()

client = hvac.Client(url=args.url, verify=args.cacert)

client.auth.approle.login(
    role_id=args.roleid, secret_id=args.secretid, mount_point=args.loginmountpoint, use_token=True)

try:
    issued_certs = client.secrets.pki.list_certificates(
        mount_point=args.certsmountpoint)
    certs = [cert for cert in issued_certs["data"]["keys"]]
except:
    certs = []

for serial in certs:
    command = [args.lib_path + "/check_ssl.sh", "-S"]
    cert_info = client.secrets.pki.read_certificate(
        serial, mount_point=args.certsmountpoint)
    pem = cert_info['data']['certificate']
    revocation_time = cert_info['data']['revocation_time']
    if revocation_time == 0:
        if args.warn is not None:
            command.append('-w ' + args.warn)
        if args.crit is not None:
            command.append('-c ' + args.crit)
        if args.afterexp is not None:
            command.append('-a ' + args.afterexp)
        queue.put([command, serial, pem])
queue.join()
check_result()
