#!/bin/bash

#set -e
#set -x
#Standard Nagios plugin return codes
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

#PASS TO UNIX HAPROXY SOCKET
SOCKET="/run/haproxy/admin.sock"

function CHECK_HAPROXY_SERVERS {
        # WORK WITH UNIX SOCKET
        SERVERS_STATS=`socat $SOCKET - <<< "show stat" |  grep "^[^#;]"`
        # WORK WITH HTTP STATS
        # SERVERS_STATS=`curl --user user:password 'http://name.of.site/stats;csv;norefresh' |  grep "^[^#;]"`

        COUNTER_DOWN=0
        COUNTER_UP=0
        # UP – transitionally DOWN
        COUNTER_UP_TO_DOWN=0
        # DISABLED THRU SOCKET
        COUNTER_MAINTENCE=0

        # Newline as separator for "for"
        IFS=$'\n'

        for i in $SERVERS_STATS; do
                SERVER_NAME=`echo \$i | cut --delimiter=, --fields=2`
                SERVER_STATE=`echo \$i | cut --delimiter=, --fields=18`
                if [[ $SERVER_NAME == "FRONTEND" ]]; then
                        if ! [[ $SERVER_STATE == "OPEN" ]]; then
                                # CHECK ACCESS TO FRONTEND
                                echo "ICINGA STATE: CRITICAL"
                                exit $STATUS_CRITICAL
                        fi
                fi
                if [[ $SERVER_STATE == MAINT ]]; then
                        COUNTER_MAINTENCE=$((COUNTER_MAINTENCE+1))
                fi
                if [[ $SERVER_STATE =~ ^UP[[:space:]][0-9]+ ]]; then
                        COUNTER_UP_TO_DOWN=$((COUNTER_UP_TO_DOWN+1))
                fi
                if [[ $SERVER_STATE == UP ]]; then
                        COUNTER_UP=$((COUNTER_UP+1))
                fi
                if [[ $SERVER_STATE =~ DOWN* ]]; then
                        COUNTER_DOWN=$((COUNTER_DOWN+1))
                fi
                OUTPUT_INF=$OUTPUT_INF"$SERVER_NAME ===> '$SERVER_STATE'\n"
        done

        OUTPUT_INF=$OUTPUT_INF"\nDOWN: $COUNTER_DOWN\nUP: $COUNTER_UP\nUP_TO_DOWN: $COUNTER_UP_TO_DOWN\nMAINTENCE: $COUNTER_MAINTENCE"
        #ICINGA STATUS
        echo -e $OUTPUT_INF
        if [[ $COUNTER_DOWN -gt 0 ]]; then
                echo "ICINGA STATE: CRITICAL"
                exit $STATUS_CRITICAL
        fi
        if [[ $COUNTER_UP_TO_DOWN -gt 0 ]]; then
                echo "ICINGA STATE: WARNING"
                exit $STATUS_WARNING
        fi
        if [[ $COUNTER_DOWN == 0 ]]; then
                echo "ICINGA STATE: OK"
                exit $STATUS_OK
        fi

}

# Main check function
CHECK_HAPROXY_SERVERS

