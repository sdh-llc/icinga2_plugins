#!/opt/icinga2_plugins/env/bin/python

import argparse
import requests
from bs4 import BeautifulSoup
import sys
import re


# Коды состояний для Nagios
OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


# Find link on static file
def find_static_link(url, static_link_regex, element_type):
    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(f"CRITICAL: Failed to retrieve page content: {e}")
        sys.exit(CRITICAL)

    try:
        soup = BeautifulSoup(response.text, 'html.parser')
        if static_link_regex != '':
            link = soup.find_all(element_type, {'href': re.compile('(%s)' % static_link_regex)})[0].get('href')
        else:
            link = soup.find_all(element_type)[0].get('href')
    except Exception as e:
        print(f"CRITICAL: Failed to parse page content: {e}")
        sys.exit(CRITICAL)

    return link

def check_static_link(static_link):
    try:
        response = requests.get(static_link)
        response.raise_for_status()
        print("static link: {}, status: {}".format(static_link, response))
    except requests.exceptions.RequestException as e:
        print(f"CRITICAL: Failed to retrieve page content: {e}")
        sys.exit(CRITICAL)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Check if the CSS file is available on the web page')
    parser.add_argument("-u", "--url", metavar='URL', type=str, help='URL of the web page to check')
    parser.add_argument("--static-link-regex", metavar='STATIC_LINK_REGEX', type=str,
                        default='',
                        help='Pattern for finding static link')
    parser.add_argument("--element-type", metavar='ELEMENT_TYPE', type=str,
                        default='link',
                        help='Element type (default: %(default)s)')

    args = parser.parse_args()

    url = args.url

    if url:
        static_link = find_static_link(args.url, args.static_link_regex, args.element_type)
        check_static_link(static_link)
    else:
        parser.print_help()
        sys.exit(UNKNOWN)
