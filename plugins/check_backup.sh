#!/bin/bash

# Standard Nagios plugin return codes.
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

CURRENT_DATE=`date +%F`
WARNING_AMOUNT=2
CRITICAL_AMOUNT=5

#HELP function
function HELP {
   NORM=`tput sgr0`
   BOLD=`tput bold`
   REV=`tput smso`
   echo -e "Help documentation for ${BOLD}$0${NORM}"\\n
   echo "${REV}-p${NORM}  --Sets the ${BOLD}path to duplicity date file."
   echo "${REV}-w${NORM}  --Sets the ${BOLD}number of days for status warning."
   echo "${REV}-c${NORM}  --Sets the ${BOLD}number of days for status critical."
   echo -e "${REV}-h${NORM}  --Displays this ${BOLD}h${NORM}elp message."\\n
   echo -e "Example: ${BOLD}$0 -p '/home/backups/log/duplicity_date.txt' -w 3 -c 6 ${NORM}"
   exit $STATUS_CRITICAL
}

#opt for script
while getopts ":p:c:w:h" opt; do
    case $opt in
      c)
        CRITICAL_AMOUNT=$OPTARG
        ;;
      w)
        WARNING_AMOUNT=$OPTARG
        ;;
      p)
        DATEFILE=`cat $OPTARG`
        ;;
      h)
         HELP
         ;;
     *)
         HELP
         ;;
        esac
    if [ -z $OPTARG ]; then
      HELP
        exit 1
    fi
done

datediff() {
    date1=$(date -d "$CURRENT_DATE" +%s)
    date2=$(date -d "$DATEFILE" +%s)
    DIFF=$(( (date1 - date2) / 86400 )) # Subtracting dates is in seconds, so we divide it by 86400 (1 day = 86400 seconds).
  }

datediff 
if [ "$DIFF" -ge "$CRITICAL_AMOUNT" ]; then
  echo "Duplicity backup older then $CRITICAL_AMOUNT days"
  echo "Last backup: $DATEFILE"
  exit $STATUS_CRITICAL
    elif [ "$DIFF" -ge "$WARNING_AMOUNT" ]; then
        echo  "Duplicity backup older then $WARNING_AMOUNT days"
        echo "Last backup: $DATEFILE"
        exit $STATUS_WARNING
    elif [ -z $DATEFILE ]; then
        echo "Duplicity backup error (empty file)"
        exit $STATUS_CRITICAL
    elif (( "$DIFF"  < 0 )); then
        echo  "Something wrong. Duplicity backup is from future?"
        echo "Last backup: $DATEFILE"
        exit $STATUS_CRITICAL   
    else
        echo "No errors"
        echo "Last backup: $DATEFILE"
        exit $STATUS_OK
fi;
