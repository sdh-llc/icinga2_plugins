#!/bin/bash

# Standard Nagios plugin return codes.
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

TIMEOUT=5s
WHISPER_PATH=/home/graphite/graphite/storage/whisper/device

function HELP {
   NORM=`tput sgr0`
   BOLD=`tput bold`
   REV=`tput smso`
   echo -e \\n"Help documentation for ${BOLD}$0${NORM}"\\n
   echo "${REV}-t${NORM}  --Sets the value for the ${BOLD}t${NORM}imeout with time suffix (s, m, h) for find."
   echo "${REV}-p${NORM}  --Sets the value for the whisper's files ${BOLD}p${NORM}ath."
   echo -e "Example: ${BOLD}$0 -t 10s -p /home/graphite/graphite/storage/whisper${NORM}"
   exit
}


while getopts :t:p:h FLAG; do
	case $FLAG in
		t)
			TIMEOUT=$OPTARG
			;;
		p)
			WHISPER_PATH=$OPTARG
			;;
		h)
			HELP
			;;

		*)
			HELP
			;;
	esac
done

if [ -d "$WHISPER_PATH" ]; then
   timeout $TIMEOUT find $WHISPER_PATH -mmin -3 -iname "*.wsp" -quit 
   if [ $? -eq 0 ]; then
      exit $STATUS_OK
   else
      echo "On path $WHISPER_PATH no "*.wsp" files with mtime lower than 3 minutes"
      exit $STATUS_CRITICAL
   fi
else
   echo "Directory doesn't exists on `hostname -f`"
   exit $STATUS_UNKNOWN
fi
