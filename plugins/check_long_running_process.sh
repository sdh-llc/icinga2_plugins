#!/bin/bash

# Standard Nagios plugin return codes.
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3


function HELP {
   NORM=`tput sgr0`
   BOLD=`tput bold`
   REV=`tput smso`
   echo -e \\n"Help documentation for ${BOLD}$0${NORM}"\\n
   echo "${REV}-D${NORM}  --Sets days."
   echo "${REV}-H${NORM}  --Sets hours."
   echo "${REV}-M${NORM}  --Sets minutes."
   echo -e "Example: ${BOLD}$0 -D 2 -H 2 -M 30 -P example${NORM} (critical when process example work more than 2 days 2 hours and 30 minutes)"
   exit
}

while getopts :P:D:H:M:h FLAG; do
	case $FLAG in
		P)
			PROCESS=$OPTARG
			;;
		D)
			DAYS=$OPTARG
			;;
		H)
			HOURS=$OPTARG
			;;
		M)
			MINUTES=$OPTARG
			;;
		h)
			HELP
			;;

		*)
			HELP
			;;
	esac
done

TIME=$((DAYS*24*60*60+HOURS*60*60+MINUTES*60))
ps hf --sort -etime -eo etimes=,pid=,comm= | grep "${PROCESS}" | while read etime pid comm; do
 if [ ${etime} -gt ${TIME} ]; then
   echo ${etime} ${pid} ${comm}
   exit $STATUS_CRITICAL
 fi
done
