#!/opt/icinga2_plugins/env/bin/python

import argparse
import logging
import sys
import ssl
import datetime
import pytz

# import requests
import urllib.request, json

logger = logging.getLogger(__name__)

STATUS_CODE_MAP = {
    'OK': 0,
    'WARNING': 1,
    'CRITICAL': 2
}

ssl_context = ssl.create_default_context()
ssl_context.check_hostname = False
ssl_context.verify_mode = ssl.CERT_NONE

def contains(url_json, params_json):
    """
    Check if 'url_json' JSON object contains 'params_json' JSON object.
    
    :param params_json: The params_jsoner JSON object to check for containment.
    :param url_json: The url_jsonr JSON object to check within.
    :return: True if 'url_json' contains 'params_json', otherwise False.
    """
    if isinstance(params_json, dict) and isinstance(url_json, dict):
        for key, value in params_json.items():
            if key not in url_json:
                return False
            if not contains(value, url_json[key]):
                return False
        return True
    elif isinstance(params_json, list) and isinstance(url_json, list):
        # Ensure all elements in 'params_json' are in 'url_json'
        return all(any(contains(elem, url_json_elem) for url_json_elem in url_json) for elem in params_json)
    else:
        return params_json == url_json

def compare_jsons(url, params_string):
    with urllib.request.urlopen(url, context=ssl_context) as url:
        url_json = json.loads(url.read().decode())
        params_json = json.loads(params_string)
        print(url_json)
        print(params_json)
        if not contains(url_json, params_json):
            return STATUS_CODE_MAP['CRITICAL']
    return STATUS_CODE_MAP['OK']

def check_status(url):
    with urllib.request.urlopen(url) as url:
        data = json.loads(url.read().decode())
        print(data)
        print("\n")
        for key, value in data.items():
            if value["status"] != "OK" and value["status"] != "collected":
                print('check ended with an error')
                return STATUS_CODE_MAP['CRITICAL']
    return STATUS_CODE_MAP['OK']



def check_status_at_specific_time(url, starttime, endtime, timezone, lastresult):
    interval_start = datetime.datetime.strptime(starttime, '%H:%M').replace(tzinfo=datetime.timezone.utc).time()
    interval_end = datetime.datetime.strptime(endtime, '%H:%M').replace(tzinfo=datetime.timezone.utc).time()
    current_time = datetime.datetime.now(pytz.timezone(timezone)).time()
    if interval_start <= current_time <= interval_end:
        return check_status(url)
    else:
      print("Check work from " + str(interval_start) +
      " till " + str(interval_end) +
      ", but now " + str(current_time))
    if 'check ended with an error' in lastresult or 'check_json.py' in lastresult:
      return check_status(url)
    return STATUS_CODE_MAP['OK']

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url",
                        help="url to json",
                        type=str, required=True, action='append')
    parser.add_argument("-s", "--status",
                        help="if only need is check status : OK", action="store_true")

    parser.add_argument("-j", "--json",
                        help="for compare link json to --json",
                        type=str, default='')

    parser.add_argument("-D", "--debug",
                        help="enable debug mode", action="store_true")

    parser.add_argument("-z", "--timezone",
                        help="set timezone",
                        type=str, default='UTC')
    parser.add_argument("-S", "--starttime",
                        help="set start point checking interval %%H:%%M",
                        type=str)
    parser.add_argument("-E", "--endtime",
                        help="set end point checking interval %%H:%%M",
                        type=str)
    parser.add_argument("-L", "--lastresult",
                        help="result of last check",
                        type=str)
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.status:
        status = 0
        if args.starttime == None or args.endtime == None:
          for url in args.url:
              status += check_status(url)
        else:
          for url in args.url:
              status += check_status_at_specific_time(url, args.starttime, args.endtime, args.timezone, args.lastresult)
        if status == 0:
            sys.exit(STATUS_CODE_MAP['OK'])
        else:
            sys.exit(STATUS_CODE_MAP['CRITICAL'])

    if args.json != '':
        status = 0
        for url in args.url:
            status += compare_jsons(url, args.json)
        if status == 0:
            sys.exit(STATUS_CODE_MAP['OK'])
        else:
            sys.exit(STATUS_CODE_MAP['CRITICAL'])

    sys.exit(STATUS_CODE_MAP['CRITICAL'])
