#!/opt/icinga2_plugins/env/bin/python
#from default_settings import *
from requests import get, exceptions
import sys
import argparse

STATUS_CODE_MAP = {
    'OK': 0,
    'WARNING': 1,
    'CRITICAL': 2
}

NODE_METRICS_KEYS = [
    'disk_free_limit',
    'disk_free_alarm',
    'disk_free',
    'fd_total',
    'fd_used',
    'mem_used',
    'mem_limit',
    'mem_alarm',

]


def get_metrics(endpoint):
    try:
        rc = get('http://%s:%s/api/%s' % (RABBITMQ_HOST, RABBITMQ_MANAGMENT_PORT, endpoint), auth=(RABBITMQ_USER,
                                                                                                   RABBITMQ_PASSWORD))
        if rc.status_code == 200:
            return rc.json()
        else:
            print("%s Error! wrong credentials?" % rc.status_code)
            sys.exit(STATUS_CODE_MAP['CRITICAL'])

    except exceptions.ConnectionError:
        print("Error getting data from broker, possibly it is down!")
        sys.exit(STATUS_CODE_MAP['CRITICAL'])


def get_queue_status(metrics):
    for param in metrics:
        if param >= max(RABBITMQ_QUEUES_LIMIT):
            print("queues reached critical state!")
            return STATUS_CODE_MAP['CRITICAL']
        if param >= min(RABBITMQ_QUEUES_LIMIT):
            print("queues reached warning state!")
            return STATUS_CODE_MAP['WARNING']

    return STATUS_CODE_MAP['OK']


def get_node_status(metrics):
    for node_name, item in metrics.items():
        # Check file discriptors
        fd_warning = item['fd_total'] * min(RABBITMQ_NODE_LIMIT) / 100
        fd_crit = item['fd_total'] * max(RABBITMQ_NODE_LIMIT) / 100
        print("fd_warning: %s" % fd_warning)
        print("fd_crit: %s" % fd_crit)
        if item['fd_used'] >= fd_crit:
            print("%s fd_used reached critical state!" % node_name)
            return STATUS_CODE_MAP['CRITICAL']
        if item['fd_used'] >= fd_warning:
            print("%s fd_used reached critical state!" % node_name)
            return STATUS_CODE_MAP['WARNING']

        # Check memory
        if item['mem_alarm']:
            return STATUS_CODE_MAP['CRITICAL']
        m_warning = item['mem_limit'] * min(RABBITMQ_NODE_LIMIT) / 100
        m_crit = item['mem_limit'] * max(RABBITMQ_NODE_LIMIT) / 100
        available_memory = (item['mem_limit'] - item['mem_used']) / 1024 / 1024
        print("mem_warning: %s" % m_warning)
        print("mem_crit: %s" % m_crit)
        print(f"Available memory left (MB): {available_memory:.2f}")
        if item['mem_used'] >= m_crit:
            print("%s mem_used reached critical state!" % node_name)
            return STATUS_CODE_MAP['CRITICAL']
        if item['mem_used'] >= m_warning:
            print("%s mem_used reached warning state!" % node_name)
            return STATUS_CODE_MAP['WARNING']
        if available_memory <= RABBITMQ_AVAILABLE_MEMORY_LIMIT[1]:  # CRITICAL
            print(f"{node_name} available memory reached critical state!")
            return STATUS_CODE_MAP['CRITICAL']
        if available_memory <= RABBITMQ_AVAILABLE_MEMORY_LIMIT[0]:  # WARNING
            print(f"{node_name} available memory reached warning state!")
            return STATUS_CODE_MAP['WARNING']

        # Check disk
        if item['disk_free_alarm']:
            return STATUS_CODE_MAP['CRITICAL']

    return STATUS_CODE_MAP['OK']


def check():
    health = get_metrics('healthchecks/node')
    if not health['status'] == 'ok':
        print(health['reason'])
        return STATUS_CODE_MAP['CRITICAL']

    queue_metrics = []
    queue_totals = get_metrics('overview')['queue_totals']
    for metric, value in queue_totals.items():
        if type(value) is not dict:
            print('%s: %s' % (metric, value))
            queue_metrics.append(value)

    nodes_data = get_metrics('nodes')
    nodes_metrics = {}
    for node in nodes_data:
        print("Node: %s" % node['name'])
        node_metrics = {}
        for key in NODE_METRICS_KEYS:
            node_metrics[key] = node[key]
            print("%s: %s" % (key, node[key]))
        nodes_metrics[node['name']] = node_metrics

    node_status = get_node_status(nodes_metrics)
    queue_status = get_queue_status(queue_metrics)
    if node_status or queue_status:
        if queue_status < node_status:
            return node_status
        else:
            return queue_status

    return STATUS_CODE_MAP['OK']

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--host", help="RabbitMQ host")
    parser.add_argument("-p", "--port", help="RabbitMQ port to connect to. (Default: 15672)")
    parser.add_argument("-U", "--user", help="User for connection")
    parser.add_argument("-P", "--password", help="User password")
    parser.add_argument("--queue-min", type=int, help="Min messages value in queue")
    parser.add_argument("--queue-max", type=int, help="Max messages value in queue")
    parser.add_argument("--node-limit-min", type=int, help="Min percent for metric")
    parser.add_argument("--node-limit-max", type=int, help="Max percent for metric")
    parser.add_argument("--available-memory-min", type=int, help="Min available memory in MB before warning")
    parser.add_argument("--available-memory-max", type=int, help="Min available memory in MB before critical")
    args = parser.parse_args()
    if args.host:
        RABBITMQ_HOST = args.host

    if args.port:
        RABBITMQ_MANAGMENT_PORT = args.port

    if args.user:
        RABBITMQ_USER = args.user

    if args.password:
        RABBITMQ_PASSWORD = args.password

    if args.queue_min and args.queue_max:
        RABBITMQ_QUEUES_LIMIT = (args.queue_min, args.queue_max)

    if args.node_limit_min and args.node_limit_max:
        RABBITMQ_NODE_LIMIT = (args.node_limit_min, args.node_limit_max)

    if args.available_memory_min and args.available_memory_max:
        RABBITMQ_AVAILABLE_MEMORY_LIMIT = (args.available_memory_min, args.available_memory_max)

    status = check()
    sys.exit(status)
