#!/opt/icinga2_plugins/env/bin/python
import pika
import sys, os, socket, struct
from optparse import OptionParser

# Constants
EXIT_NAGIOS_OK = 0
EXIT_NAGIOS_WARN = 1
EXIT_NAGIOS_CRITICAL = 2


opt_parser = OptionParser()
opt_parser.add_option("-H", "--host", dest="server", default='127.0.0.1', help="RabbitMQ server to connect to via protocol ampq.")
opt_parser.add_option("-p", "--port", dest="port", type=int, default=5672, help="RabbitMQ port to connect to. (Default: 5672)")
opt_parser.add_option("-P", "--password", dest="password", default=None, help="RabbitMQ password to use.")
opt_parser.add_option("-U", "--user", dest="user", default=False, help="User for connection")
opt_parser.add_option("-D", "--vhost", dest="vhost", default=False, help="vhost")
#opt_parser.add_option("-S", "--ssl", dest="ssl", default=False, action="store_true", help="Enable secure communication. (Default: False)")
#opt_parser.add_option("-t", "--timeout", dest="blocked_connection_timeout", default=30000, type=int, help="How many seconds to wait for host to respond.")
args = opt_parser.parse_args()[0]

host = args.server
port = args.port
password = args.password
user = args.user
vhost = args.vhost

credentials = pika.PlainCredentials(user,password)

try:
    connection = pika.BlockingConnection(pika.ConnectionParameters(host, port,vhost,credentials))
    if connection:
        print 'OK: RabbitMQ works on host: %s port: %s '% (host , port)
        connection.close()
        sys.exit(EXIT_NAGIOS_OK)
    else:
        print 'CRITICAL: RabbitMQ not respond on port: %s port: %s '% (host , port)
        connection.close()
        sys.exit(EXIT_NAGIOS_CRITICAL)
    #channel = connection.channel()

    #channel.queue_declare(queue='hello')
except (pika.exceptions.ConnectionClosed),e:
    print 'CRITICAL: RabbitMQ not respond on port: %s port: %s '% (host , port)
    sys.exit(EXIT_NAGIOS_CRITICAL)


