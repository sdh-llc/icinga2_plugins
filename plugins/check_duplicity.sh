#!/bin/bash

# Standard Nagios plugin return codes
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

#path to bin files
TAC="/usr/bin/tac"
GREP="/bin/grep"
AWK="/usr/bin/awk"
PRINTF="/usr/bin/printf"

#HELP function
function HELP {
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`
echo -e "Help documentation for ${BOLD}$0${NORM}"\\n
echo "${REV}-p${NORM}  --Sets the ${BOLD}path to duplicity log."
echo -e "${REV}-h${NORM}  --Displays this ${BOLD}h${NORM}elp message."\\n
echo -e "Example: ${BOLD}$0 -p '/var/log/duplicity.log' ${NORM}"
exit
}

function CheckBackup {

BACKUP_LOG_DATA=$(${TAC} "$directory" | ${GREP} "\[ Backup Statistics \]" -B 15)
BACKUP_DATE_START=$(${PRINTF} "%b" "$BACKUP_LOG_DATA" | ${GREP} StartTime | ${AWK} -F"[StartTime] " '{print $2}' | ${GREP} -o -E '[0-9]{10}')
ERROR=$(${PRINTF} "%b" "$BACKUP_LOG_DATA" | ${GREP} Errors | ${GREP} -o -E '[0-9]')
CURRENT_DAY=$(date +"%s")

#add to backup date 1 day
TMP_BACKUP_DATE_START=$(date -d "$(date --date='@'$BACKUP_DATE_START'')+1 days")

#formatting dates
CURRENT_DATE=$(date -d @"$CURRENT_DAY" +"%d" )
BACKUP_DATE_START=$(date -d "$TMP_BACKUP_DATE_START" +"%d")

#delta of previous day
DELTA=$(echo $(($CURRENT_DATE - $BACKUP_DATE_START)))


#check if backup was run yesterday
if [ $DELTA -ne 0 ]; then
    echo "problems"
    ${PRINTF} "%b" "$BACKUP_LOG_DATA"
    exit $STATUS_CRITICAL
fi

#check if we have errors when backup is going

if [ $ERROR -eq 0 ]; then
    ${PRINTF} "%b" "$BACKUP_LOG_DATA"
    exit $STATUS_OK
    else
    ${PRINTF} "%b" "$BACKUP_LOG_DATA"
    exit $STATUS_CRITICAL
fi

}



#opt for script
while getopts ":p:h" opt; do
    case $opt in
        p)
        directory=$OPTARG
        CheckBackup
        ;;
        h)
	    HELP
	    ;;
	    *)
	    HELP
	    ;;
        esac
    if [ -z $OPTARG ]; then
	    HELP
        exit 1

    fi
done

