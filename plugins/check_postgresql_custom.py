#!/opt/icinga2_plugins/env/bin/python

import psycopg2
import subprocess
import re
import sys
from optparse import OptionParser

# Constants
EXIT_NAGIOS_OK = 0
EXIT_NAGIOS_WARN = 1
EXIT_NAGIOS_CRITICAL = 2

# Command line options
opt_parser = OptionParser()
opt_parser.add_option("-H", "--host", dest="server", default='127.0.0.1', help="Postgresql address.")
opt_parser.add_option("-p", "--port", dest="port", default=5432, help="Postgresql port to connect to. (Default: 5432)")
opt_parser.add_option("-D", "--database", dest="database", default="test",
                      help="Postgresql database to use. Defaults is test.")
opt_parser.add_option("-U", "--user", dest="user", default="test",
                      help="Postgresql user to use. Defaults to unauthenticated. Defaults is test")
opt_parser.add_option("-P", "--password", dest="password", default="test",
                      help="Postgresql password to use. Defaults to unauthenticated. Defaults is test")
opt_parser.add_option("-t", "--timeout", dest="timeout", default=10, type=int,
                      help="How many seconds to wait for host to respond.")
args = opt_parser.parse_args()[0]

regex_success = r"password authentication failed for user"
regex_error = r"Is the server running on host"
regex_error2 = r"the database system is shutting down"
regex_pgHbaConf = r"no pg_hba.conf entry for host"
connection = ' '
try:
    connection = psycopg2.connect(
        database=args.database,
        user=args.user,
        password=args.password,
        host=args.server,
        port=args.port)

except psycopg2.OperationalError as e:
    try:
        matches_success = re.findall(regex_success, str(e))
        matches_error = re.findall(regex_error, str(e))
        matches_error2 = re.findall(regex_error2, str(e))
        matches_errorHbaConf = re.findall(regex_pgHbaConf, str(e))
        if matches_success:
            print('Postgresql running on address %s and port %s' % (args.server, args.port))
            sys.exit(EXIT_NAGIOS_OK)
        elif matches_error:
            print('Postgresql NOT running on address %s and port %s' % (args.server, args.port))
            sys.exit(EXIT_NAGIOS_CRITICAL)
        elif matches_error2:
            print('Postgresql is shutting down on address %s and port %s' % (args.server, args.port))
            sys.exit(EXIT_NAGIOS_CRITICAL)
        elif matches_errorHbaConf:
            print('Postgresql no pg_hba.conf entry for host %s:%s , for user %s and database %s' % (
            args.server, args.port, args.user, args.database))
            sys.exit(EXIT_NAGIOS_OK)
        else:
            print(e)
            sys.exit(EXIT_NAGIOS_CRITICAL)
    except re.error as e:
        print(e)
        sys.exit(EXIT_NAGIOS_CRITICAL)
