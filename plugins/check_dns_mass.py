#!/opt/icinga2_plugins/env/bin/python

import sys
import subprocess
import json
try:
    from queue import Queue
except:
    from Queue import Queue
import threading
import time
import argparse
import os.path

lib_path = "/opt/icinga2_plugins/plugins"

EXIT_NAGIOS_OK = 0
EXIT_NAGIOS_WARN = 1
EXIT_NAGIOS_CRITICAL = 2

number_of_tread = 4
end_result = []


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-d", "--dns_record", help='''mass a record check example for icinga:
[
    {"hostname": "my.terneo.ua", "expected_address": "116.203.127.197"},
    {"hostname": "google.com", "expected_address": "8.8.8.8"},
    {"hostname": "api.terneo.ua", "expected_address": "116.203.127.197"}
]
if you run it in linux console, use next format -s '[{},{}]' example:\
''' + sys.argv[0] + ''' -c \'[{"hostname": "my.terneo.ua",\
     "expected_address": "116.203.127.197"}]\'
all parameters that script parses (please use check_ssl.sh for helping):
    hostname
    expected_address
    ''', type=str, action="append")

    parser.add_argument("-l", "--lib_path", help='''you can provide full path to check_dns \
and check_ssl.sh example -l ./ ''', type=str, action="append")

    args = parser.parse_args()

if args.lib_path is not None:
    lib_path = args.lib_path[0]
    if not os.path.isfile(lib_path + "/check_dns"):
        print(lib_path)
        print("please check the provided lib_path")
        sys.exit(EXIT_NAGIOS_CRITICAL)

# check if all needed scripts are presented
if not os.path.isfile(lib_path + "/check_dns"):
    print(("please check check_dns scripts in " + lib_path +
          " or change lib_path for local testing -l new_lib_path"))
    sys.exit(EXIT_NAGIOS_CRITICAL)


class MultiProc(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            data = self.queue.get()
            self.get_data(data)
            self.queue.task_done()

    def get_data(self, data):
        run_command(data)


def check_result():
    final_status = 0
    for i in end_result:
        if i['status'] > final_status:
            final_status = i['status']
    newlist = sorted(end_result, key=lambda k: k['status'], reverse=True)
    for i in newlist:
        print((i['name'], i['text']))
    if final_status == 0:
        sys.exit(EXIT_NAGIOS_OK)
    if final_status == 1:
        sys.exit(EXIT_NAGIOS_WARN)
    if final_status == 2:
        sys.exit(EXIT_NAGIOS_CRITICAL)
    if final_status == 3:
        sys.exit(EXIT_NAGIOS_CRITICAL)


def run_command(data):
    command = data[0]
    status = 0
    name = data[1]
    try:
        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        result = str(proc.stdout.read())

    except subprocess.CalledProcessError as e:
        result = str(e.output)
    if 'OK:' not in result:
        if 'WARNING' in result:
            status = 1
        elif 'CRITICAL' in result:
            status = 2
        else:
            status = 3
    end_result.append({'name': name, 'status': status, 'text': result.replace(
        "\\n", "").replace("\n", "")})

queue = Queue()
for i in range(number_of_tread):
    t = MultiProc(queue)
    t.daemon = True
    t.start()
    d = args.dns_record[0]
    dns_records = json.loads(d.replace('}, ]', '} ]'))
    # print(dns_records)
    for i in dns_records:
        command = [lib_path + "/check_dns"]
        if 'hostname' in i:
            command.append("-H")
            command.append(str(i['hostname']))
            name = str(i['hostname'])
        if 'expected_address' in i:
            command.append("-a")
            command.append(str(i['expected_address']))
            name = str(i['expected_address'])
        try:
            queue.put([command, name])
        except NameError:
            print('hostname or expected_address should be provided ')
            sys.exit(EXIT_NAGIOS_CRITICAL)
    queue.join()
    check_result()
