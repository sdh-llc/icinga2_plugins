#!/opt/icinga2_plugins/env/bin/python

import sys
import subprocess
import json
try:
    from queue import Queue
except:
    from Queue import Queue
import threading
import time
import argparse
import os.path

lib_path = "/usr/lib/nagios/plugins"

EXIT_NAGIOS_OK = 0
EXIT_NAGIOS_WARN = 1
EXIT_NAGIOS_CRITICAL = 2

number_of_tread = 4
end_result = []


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-s", "--site", help='''mass http check example for icinga:
[
    {"http_address": "my.terneo.ua", "http_port": 443,\
     "http_expect": 200, "http_ssl": "true"},
    {"http_address": "google.com"},
    {"http_address": "api.terneo.ua", "http_port": 443,\
     "http_expect": "403,200", "http_ssl": "true"}
]
if you run it in linux console, use next format -s '[{},{}]' example: \
''' + sys.argv[0] + ' -s \'[{"http_address": "my.terneo.ua"}]\'''''
all parameters that script parses (please use check_http for helping):
    http_address
    http_vhost
    http_expect
    http_ssl
    http_port
    http_auth_pair
    http_critical_time
    http_headerstring
    http_sni
    http_ipv4
    http_ipv6
    http_onredirect
    http_timeout
    mandatory - http_address or http_vhost
        ''',
     type=str, action="append")

    parser.add_argument("-c", "--cert", help='''mass ssl check example for icinga:
[
    {"ssl_address": "my.terneo.ua", "ssl_cert_valid_days_warn": 50,\
     "ssl_cert_valid_days_critical": "30", "http_port": 443},
    {"ssl_address": "google.com", "ssl_cert_valid_days_warn": 30,\
     "ssl_cert_valid_days_critical": "10", "http_port": 443},
    {"ssl_address": "api.terneo.ua", "ssl_cert_valid_days_warn": 60,\
     "ssl_cert_valid_days_critical": "40", "http_port": 443}
]
if you run it in linux console, use next format -s '[{},{}]' example:\
''' + sys.argv[0] + ''' -c \'[{"ssl_address": "my.terneo.ua",\
     "ssl_cert_valid_days_warn": 50, "ssl_cert_valid_days_critical": "30"}]\'
all parameters that script parses (please use check_ssl.sh for helping):
    ssl_ip_address
    ssl_address
    ssl_port
    ssl_cert_valid_days_warn
    ssl_cert_valid_days_critical
    ''', type=str, action="append")

    parser.add_argument("-l", "--lib_path", help='''you can provide full path to check_http \
and check_ssl.sh example -l ./ ''', type=str, action="append")

    args = parser.parse_args()

if args.lib_path is not None:
    lib_path = args.lib_path[0]
    if not os.path.isfile(lib_path + "/check_http") or \
            not os.path.isfile(lib_path + "/check_ssl.sh"):
        print(lib_path)
        print("please check the provided lib_path")
        sys.exit(EXIT_NAGIOS_CRITICAL)

# check if all needed scripts are presented
if not os.path.isfile(lib_path + "/check_http") or \
        not os.path.isfile(lib_path + "/check_ssl.sh"):
    print(("please check check_http and check_ssl.sh scripts in " + lib_path +
          " or change lib_path for local testing -l new_lib_path"))
    sys.exit(EXIT_NAGIOS_CRITICAL)


class MultiProc(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            data = self.queue.get()
            self.get_data(data)
            self.queue.task_done()

    def get_data(self, data):
        run_command(data)


def check_result():
    final_status = 0
    for i in end_result:
        if i['status'] > final_status:
            final_status = i['status']
    newlist = sorted(end_result, key=lambda k: k['status'], reverse=True)
    for i in newlist:
        print((i['name'], i['text']))
    if final_status == 0:
        sys.exit(EXIT_NAGIOS_OK)
    if final_status == 1:
        sys.exit(EXIT_NAGIOS_WARN)
    if final_status == 2:
        sys.exit(EXIT_NAGIOS_CRITICAL)
    if final_status == 3:
        sys.exit(EXIT_NAGIOS_CRITICAL)


def run_command(data):
    command = data[0]
    status = 0
    name = data[1]
    try:
        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        result = str(proc.stdout.read())

    except subprocess.CalledProcessError as e:
        result = str(e.output)
    if 'OK:' not in result:
        if 'WARNING' in result:
            status = 1
        elif 'CRITICAL' in result:
            status = 2
        else:
            status = 3
    end_result.append({'name': name, 'status': status, 'text': result.replace(
        "\\n", "").replace("\n", "")})

if args.site is not None or args.cert is not None:
    queue = Queue()
    for i in range(number_of_tread):
        t = MultiProc(queue)
        t.daemon = True
        t.start()

    if args.site is not None:
        s = args.site[0]
        sites = json.loads(s.replace('}, ]', '} ]'))
#        print(sites)
        for i in sites:
            command = [lib_path + "/check_http"]
            if 'http_address' in i:
                command.append("-I")
                command.append(str(i['http_address']))
                name = str(i['http_address'])
            if 'http_vhost' in i:
                command.append("-H")
                command.append(str(i['http_vhost']))
                name = str(i['http_vhost'])
            if 'http_expect' in i:
                command.append('-e ' + str(i['http_expect']))
            if 'http_ssl' in i and i['http_ssl'] == "true":
                command.append('-S')
            if 'http_port' in i:
                command.append('-p ' + str(i['http_port']))
            if 'http_url' in i:
                command.append("-u " + str(i['http_url']))
            if 'http_auth_pair' in i:
                command.append("-a " + str(i['http_auth_pair']))
            if 'http_critical_time' in i:
                command.append("-c " + str(i['http_critical_time']))
            if 'http_headerstring' in i:
                command.append('-k ' + str(i['http_headerstring']))
            if 'http_sni' in i:
                command.append('--sni')
            if 'http_ipv4' in i:
                command.append('-4')
            if 'http_ipv6' in i:
                command.append('-6')
            if 'http_onredirect' in i:
                command.append('-f')
                command.append( str(i['http_onredirect']))
            if 'http_timeout' in i:
                command.append('-t ' + str(i['http_timeout']))
            try:
                queue.put([command, name])
            except NameError:
                print('http_address or http_vhost should be provided ')
                sys.exit(EXIT_NAGIOS_CRITICAL)

    elif args.cert is not None:
        s = args.cert[0]
        certs = json.loads(s.replace('}, ]', '} ]'))
        for i in certs:
            command = [lib_path + "/check_ssl.sh"]
            if 'ssl_ip_address' in i:
                command.append("-I")
                command.append(str(i['ssl_ip_address']))
                name = str(i['ssl_ip_address'])
            if 'ssl_address' in i:
                command.append("-H")
                command.append(str(i['ssl_address']))
                name = str(i['ssl_address'])
            if 'ssl_port' in i:
                command.append('-p ' + str(i['ssl_port']))
            if 'days_warn' in i:
                command.append('-w ' + str(i['days_warn']))
            if 'days_crit' in i:
                command.append('-c ' + str(i['days_crit']))
            try:
                queue.put([command, name])
            except NameError:
                print('ssl_ip_address or ssl_address should be provided ')
                sys.exit(EXIT_NAGIOS_CRITICAL)
    queue.join()
    check_result()
else:
    print((sys.argv[0]))
    print("You dont provide list of servers. Please use -h option for help")
    sys.exit(-1)
