#!/bin/bash
# Standard Nagios plugin return codes
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

function HELP {
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`
echo -e \\n"Help documentation for ${BOLD}$0${NORM}"\\n
echo "${REV}-c${NORM}  --Sets ${BOLD}your custom command."
echo "${REV}-O${NORM}  --Sets the ${BOLD}exspression that means status OK."
echo "${REV}-E${NORM}  --Sets the ${BOLD}exspression that means status CRITICAL."
echo "${REV}-W${NORM}  --Sets the ${BOLD}exspression that means status WARNING."
echo -e "Example: ${BOLD}$0 -c <custom command> -O <ok result> -W <warning result> -E <critical result>${NORM}"
echo -e "Example: ${BOLD}$0 -c \"./test.sh\" -O ok -W warning -E alarm${NORM}"

exit $STATUS_UNKNOWN
}

while getopts :c:O:E:W:h FLAG; do
        case $FLAG in
                c)
                        COMMAND=$OPTARG
                        ;;
                O)
                        OK=$OPTARG
                        ;;
                E)
                        CRITICAL=$OPTARG
                        ;;
                W)      
                        WARNING=$OPTARG
                        ;;   
                h)
                        HELP
                        ;;

                        *)
                        HELP
                        ;;
        esac
done

if [[ -z ${OK} || -z ${COMMAND} ]]; then
        HELP
else
    RESULT=$(sudo $COMMAND)
    VALUE=$(echo "${RESULT}" | grep $OK | wc -l)
    if [[ "${VALUE}" -ge 1  ]]; then
        echo "OK, State is : ${RESULT}"
        exit $STATUS_OK
    else
        if [[ -n ${WARNING} ]]; then
        VALUE=$(echo "${RESULT}" | grep $WARNING | wc -l)
            if [[ "${VALUE}" -ge 1 ]]; then
                echo "WARNING, State is : ${RESULT}"
                exit $STATUS_WARNING
            else          
                if [[ -n ${CRITICAL} ]]; then
                VALUE=$(echo "${RESULT}" | grep $CRITICAL | wc -l)
                    if [[ "${VALUE}" -ge 1 ]]; then
                        echo "ERROR, State is : ${RESULT}"
                        exit $STATUS_CRITICAL
                    else
                        echo "ERROR"
                        exit $STATUS_CRITICAL
                    fi
                fi
            fi
        fi 
    fi
fi