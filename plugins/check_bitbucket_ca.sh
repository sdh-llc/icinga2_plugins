#!/bin/bash

set -e

cd /var/lib/nagios/fh-ca && git pull

CERT_LIST=$(find /var/lib/nagios/fh-ca/ -name '*.crt')
for i in "${CERT_LIST[@]}"
do
   /opt/icinga2_plugins/plugins/check_ssl.sh '-c' '30' '-w' '90' -F "$i"
done
