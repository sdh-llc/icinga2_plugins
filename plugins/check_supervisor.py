#!/opt/icinga2_plugins/env/bin/python

# Constants
EXIT_NAGIOS_OK = 0
EXIT_NAGIOS_WARN = 1
EXIT_NAGIOS_CRITICAL = 2

regForSuper="^([a-zA-Z0-9-_]+)\s+(\w+)\s+(can't find command '[a-zA-z0-9\/-]+| pid\s\d+,\suptime\s\d+\s\w+,\s\d+:\d+:\d+)"

# Command line options
opt_parser = OptionParser()
opt_parser.add_option("-a", "--list-all", dest="list-all", default=True, help="This parameter check all daemons")
opt_parser.add_option("-d", "--daemons", dest="daemons", default="", help="Type list of needed daemons")
args = opt_parser.parse_args()[0]
