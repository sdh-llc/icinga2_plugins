#!/bin/bash

# Standard Nagios plugin return codes.
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

PORT=1718

#HELP function
function HELP {
   NORM=`tput sgr0`
   BOLD=`tput bold`
   REV=`tput smso`
   echo -e "Help documentation for ${BOLD}$0${NORM}"\\n
   echo "${REV}-w${NORM}  --Sets the ${BOLD}number of days for status warning."
   echo "${REV}-c${NORM}  --Sets the ${BOLD}number of days for status critical."
   echo "${REV}-p${NORM}  --Sets the ${BOLD}port with stats."   
   echo -e "${REV}-h${NORM}  --Displays this ${BOLD}h${NORM}elp message."\\n
   echo -e "Example: ${BOLD}$0 -w 3 -c 6 -p 1719 ${NORM}"
   exit $STATUS_UNKNOWN
}

#opt for script
while getopts ":p:c:w:h" opt; do
    case $opt in
      c)
        CRITICAL_AMOUNT=$OPTARG
        ;;
      w)
        WARNING_AMOUNT=$OPTARG
        ;;
      p)
        PORT=$OPTARG
        ;;
      h)
         HELP
         ;;
     *)
         HELP
         ;;
        esac
    if [ -z $OPTARG ]; then
      HELP
        exit 1
    fi
done

UWSGI_STATS=$(curl -s http://127.0.0.1:"$PORT")
IDLE_WORKERS=$(echo "$UWSGI_STATS" | grep -A 3 core.idle_workers | grep value | cut -d':' -f2)
BUSY_WORKERS=$(echo "$UWSGI_STATS" | grep -A 3 core.busy_workers | grep value | cut -d':' -f2)
NUBMER_OF_WORKERS=$(($IDLE_WORKERS + $BUSY_WORKERS ))

if [ -z $WARNING_AMOUNT ]; then
    WARNING_AMOUNT=$(($NUBMER_OF_WORKERS - 3))
fi
if [ -z $CRITICAL_AMOUNT ]; then
    CRITICAL_AMOUNT=$(($NUBMER_OF_WORKERS - 1))
fi

if [ "$NUBMER_OF_WORKERS" == 0 ]; then
  echo "Something wrong, can\`t find workers"
  echo "Please, check if stat is enabled in uwsgi.ini file"
  exit $STATUS_CRITICAL
    elif [ "$BUSY_WORKERS" -ge "$CRITICAL_AMOUNT" ]; then
        echo "Number of busy workers - $BUSY_WORKERS"
        echo "Number of workers - $NUBMER_OF_WORKERS"
        exit $STATUS_CRITICAL
    elif [ "$BUSY_WORKERS" -ge "$WARNING_AMOUNT" ]; then
        echo "Number of busy workers - $BUSY_WORKERS"
        echo "Number of workers - $NUBMER_OF_WORKERS"
        exit $STATUS_WARNING  
    else    
        echo "Number of busy workers - $BUSY_WORKERS"
        echo "Number of workers - $NUBMER_OF_WORKERS"
        exit $STATUS_OK
fi;
