#!/bin/bash
# Standard Nagios plugin return codes
STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3
STATUS=0
{
    asd=$(dpkg -s smartmontools)

} || {
    echo "please install smartmontools "
    exit $STATUS_UNKNOWN
}

SCAN=$(sudo smartctl --scan)

MEGARAID=$(echo  "$SCAN" | grep "megaraid" | wc -l)

if [[ "${MEGARAID}" -ge 1 ]]; then
        LIST_DEVICES=$(echo "${SCAN}" | grep "megaraid")
        COUNT="${MEGARAID}"
else
        LIST_DEVICES="${SCAN}"
        COUNT=$(echo  "${SCAN}" | wc -l)
fi 

for i in  $(seq 1 "${COUNT}")
do
        if [[ "${MEGARAID}" -ge 1 ]]; then
                args=$(echo "${LIST_DEVICES}"| head -n $i |tail -n 1| awk -F" " '{print $1,$2,$3}')
        else
                args=$(echo "${LIST_DEVICES}"| head -n $i |tail -n 1| awk -F" " '{print $1}')
        fi
        asd=$(sudo smartctl -H ${args})
        res=$(echo "${asd}" | grep -v "www.smartmontools.org" | grep -v "local build" | grep -v "===")
        state=$(echo "${res}" | grep PASSED | wc -l)
        if [[ "${state}" -eq 1 ]];then
                STATE="OK:"
        else
                STATE="CRITICAL:"
                STATUS=2
        fi
        RESULT[$i]="${STATE}${args}${res}"
        echo ${RESULT[$i]}
done

if [[ $STATUS -eq 0 ]]; then
        exit $STATUS_OK
else
        exit $STATUS_CRITICAL

fi
